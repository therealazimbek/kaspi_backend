import datetime

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

from decimal import Decimal
from django.core.validators import MinValueValidator, MaxValueValidator
from api.validators import *


class UserManager(BaseUserManager):
    def create_user(self, email, phone, first_name, last_name, password=None):
        if not email:
            raise ValueError('Users must have email address')
        if not phone:
            raise ValueError('Users must have phone number')

        user = self.model(
            email=self.normalize_email(email),
            phone=phone,
            first_name=first_name,
            last_name=last_name
        )

        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, phone, first_name, last_name, password=None):
        user = self.create_user(
            email=self.normalize_email(email),
            password=password,
            phone=phone,
            first_name=first_name,
            last_name=last_name
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True

        user.save()
        return user


class User(AbstractBaseUser):
    email = models.EmailField(verbose_name="email", unique=True)
    phone = models.CharField(max_length=12, verbose_name="phone number", unique=True)
    username = models.CharField(max_length=80, unique=True)
    date_joined = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
    last_login = models.DateTimeField(verbose_name='last login', auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    first_name = models.CharField(max_length=60)
    last_name = models.CharField(max_length=80)
    age = models.DateField(default=datetime.date.today)
    avatar = models.ImageField(upload_to='image', blank=True, null=True, default='avatar.svg')

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['email', 'first_name', 'last_name']

    objects = UserManager()

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app):
        return self.is_admin


class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Product(models.Model):
    title = models.CharField(max_length=150)
    description = models.TextField(null=True, blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    available_inventory = models.PositiveIntegerField()
    average_rating = models.DecimalField(default=0, max_digits=1, decimal_places=1)
    votes = models.PositiveIntegerField(default=0)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-updated', '-created']

    def __str__(self):
        return self.title


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    body = models.TextField()
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-updated', '-created']

    def __str__(self):
        return self.body[0:50]


class Cart(models.Model):
    customer = models.OneToOneField(User, related_name='cart', on_delete=models.CASCADE)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)


class CartItem(models.Model):
    cart = models.ForeignKey(Cart, related_name='items', on_delete=models.CASCADE, null=True, blank=True)
    product = models.ForeignKey(Product, related_name='items', on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1, null=True, blank=True)

    def __unicode__(self):
        return '%s: %s' % (self.product.title, self.quantity)

    def __str__(self):
        return '%s: %s' % (self.product.title, self.quantity)


class Order(models.Model):
    customer = models.ForeignKey(User, related_name='orders', on_delete=models.CASCADE, null=True, blank=True)
    total = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='order_items', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='order_items', on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(null=True, blank=True)

    def __unicode__(self):
        return '%s: %s' % (self.product.title, self.quantity)

    def __str__(self):
        return '%s: %s' % (self.product.title, self.quantity)


class Card(models.Model):
    card_number = models.BigIntegerField(unique=True, validators=[length_16, validate_credit_card])
    card_cvs = models.IntegerField(validators=[length_3])
    balance = models.DecimalField(max_digits=16, decimal_places=2)
    pincode = models.IntegerField(validators=[length_4])


class GoldCard(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    card_number = models.BigIntegerField(unique=True, validators=[length_16, validate_credit_card])
    card_cvs = models.IntegerField(validators=[length_3])
    balance = models.DecimalField(max_digits=16, decimal_places=2)
    pincode = models.IntegerField(validators=[length_4])


class BonusCard(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    balance = models.DecimalField(max_digits=16, decimal_places=2)


class DebtCard(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    start_date = models.DateTimeField(auto_now_add=True)
    total = models.DecimalField(max_digits=16, decimal_places=2)
    repay = models.DecimalField(max_digits=16, decimal_places=2)
    remains = models.DecimalField(max_digits=16, decimal_places=2)
    end_date = models.DateTimeField()


class UserBank(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class GoldCardTransfer(models.Model):
    from_card = models.BigIntegerField(validators=[length_16])
    to_card = models.BigIntegerField(validators=[length_16])
    amount = models.DecimalField(max_digits=16, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])
    created = models.DateTimeField(auto_now_add=True)


class GoldCardPayment(models.Model):
    info = models.CharField(max_length=200)
    from_card = models.ForeignKey(GoldCard, on_delete=models.CASCADE, related_name='card')
    amount = models.DecimalField(max_digits=16, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])
    created = models.DateTimeField(auto_now_add=True)


class PaymentCategory(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Payment(models.Model):
    title = models.CharField(max_length=150)
    category = models.ForeignKey(PaymentCategory, on_delete=models.SET_NULL, null=True)
    amount = models.DecimalField(max_digits=16, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])
    card = models.ForeignKey(GoldCard, on_delete=models.CASCADE, related_name='fromCard')
    description = models.TextField(null=True, blank=True)
    account_number = models.PositiveIntegerField(null=True, blank=True, validators=[MaxValueValidator(999999999), MinValueValidator(100000000)])
    phone_number = models.CharField(null=True, blank=True, max_length=12)