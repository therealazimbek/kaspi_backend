from django.contrib import admin
from .models import *

admin.site.register(User)
admin.site.register(Comment)
admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Cart)
admin.site.register(CartItem)
admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(GoldCard)
admin.site.register(BonusCard)
admin.site.register(DebtCard)
admin.site.register(GoldCardTransfer)
admin.site.register(GoldCardPayment)
admin.site.register(PaymentCategory)
admin.site.register(Payment)
