from django.urls import path, include
from rest_framework import routers

from .views import *

router = routers.DefaultRouter()
router.register('carts', CartViewSet, basename='carts')
router.register('products', ProductViewSet, basename='products')
router.register('comments', CommentViewSet, basename='comments')
router.register('category', CategoryViewSet, basename='category')
router.register('orders', OrderViewSet, basename='orders')
router.register('card', CardViewSet, basename='card')
router.register('gold_card', GoldCardViewSet, basename='gold_card')
router.register('bonus_card', BonusCardViewSet, basename='bonus_card')
router.register('payment_category', PaymentCategoryViewSet, basename='payment_category')

urlpatterns = [
    path('login/', MyTokenObtainPairView.as_view()),
    path('users/', UserView.as_view()),

    path('cart_items/', CartItemViewSet.as_view(dict(get='list', post='create'))),
    path('cart_items/<int:pk>/', CartItemViewSet.as_view(dict(get='retrieve', delete='destroy', put='update'))),

    path('order_items/', OrderItemViewSet.as_view(dict(get='list', post='create'))),
    path('order_items/<int:pk>/', OrderItemViewSet.as_view(dict(get='retrieve', delete='destroy', put='update'))),

    path('transfer/', GoldCardTransferViewSet.as_view()),
    path('payment/', PaymentAPI.as_view()),

    path('', include(router.urls)),
]
