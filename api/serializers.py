from abc import ABC

from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from .models import *


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        token['firstName'] = user.first_name
        token['lastName'] = user.last_name
        token['phone'] = user.phone
        return token


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'phone', 'first_name', 'last_name', 'email', 'age', 'avatar']


class RegisterSerializer(serializers.ModelSerializer):
    phone = serializers.CharField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ['phone', 'password', 'password2', 'email', 'first_name', 'last_name']
        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True}
        }

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            phone=validated_data['phone'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            username=validated_data['first_name'] + validated_data['last_name']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'name']


class ProductSerializer(serializers.ModelSerializer):
    category = CategorySerializer(read_only=True)
    category_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Product
        fields = ['id', 'title', 'description', 'price', 'available_inventory', 'average_rating', 'votes', 'category',
                  'category_id']


class CommentSerializer(serializers.ModelSerializer):
    product = ProductSerializer(read_only=True)
    product_id = serializers.IntegerField(write_only=True)
    user = UserSerializer(read_only=True)
    user_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Comment
        fields = ['id', 'user', 'user_id', 'product', 'product_id', 'body', 'updated', 'created']


class CartSerializer(serializers.ModelSerializer):
    customer = UserSerializer(read_only=True)
    customer_id = serializers.IntegerField(write_only=True)
    items = serializers.StringRelatedField(many=True)

    class Meta:
        model = Cart
        fields = ['id', 'customer', 'customer_id', 'created', 'updated', 'items']


class CartItemSerializer(serializers.ModelSerializer):
    cart = CartSerializer(read_only=True)
    product = ProductSerializer(read_only=True)

    class Meta:
        model = CartItem
        fields = ['id', 'cart', 'product', 'quantity']


class OrderSerializer(serializers.ModelSerializer):
    customer = UserSerializer(read_only=True)
    order_items = serializers.StringRelatedField(many=True, required=False)

    class Meta:
        model = Order
        fields = ['id', 'customer', 'total', 'created', 'updated', 'order_items']

    def create(self, validated_data):
        order = Order.objects.create(**validated_data)
        return order


class OrderItemSerializer(serializers.ModelSerializer):
    order = OrderSerializer(read_only=True)
    product = ProductSerializer(read_only=True)

    class Meta:
        model = OrderItem
        fields = ['id', 'order', 'product', 'quantity']


class CardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Card
        fields = ('id', 'card_number', 'card_cvs', 'balance', 'pincode')


class GoldCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = GoldCard
        fields = ('id', 'user', 'card_number', 'card_cvs', 'balance', 'pincode')


class BonusCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = BonusCard
        fields = ('id', 'user', 'balance')


class DebtCard(serializers.ModelSerializer):
    class Meta:
        model = DebtCard
        field = ('id', 'user', 'start_date', 'total', 'repay', 'remains', 'end_date')


class GoldCardTransferSerializer(serializers.ModelSerializer):
    class Meta:
        model = GoldCardTransfer
        fields = ('id', 'from_card', 'to_card', 'amount', 'created')


class PaymentCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentCategory
        fields = '__all__'


class PaymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Payment
        fields = '__all__'


class GoldCardPaymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = GoldCardPayment
        fields = '__all__'
