# Generated by Django 4.1.4 on 2022-12-21 10:02

import api.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_paymentcategory_payment_goldcardpayment'),
    ]

    operations = [
        migrations.CreateModel(
            name='Card',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('card_number', models.BigIntegerField(unique=True, validators=[api.validators.length_16])),
                ('card_cvs', models.IntegerField(validators=[api.validators.length_3])),
                ('balance', models.DecimalField(decimal_places=2, max_digits=16)),
                ('pincode', models.IntegerField(validators=[api.validators.length_4])),
            ],
        ),
        migrations.AlterField(
            model_name='goldcard',
            name='card_number',
            field=models.BigIntegerField(unique=True, validators=[api.validators.length_16]),
        ),
        migrations.AlterField(
            model_name='goldcardtransfer',
            name='from_card',
            field=models.BigIntegerField(validators=[api.validators.length_16]),
        ),
        migrations.AlterField(
            model_name='goldcardtransfer',
            name='to_card',
            field=models.BigIntegerField(validators=[api.validators.length_16]),
        ),
    ]
