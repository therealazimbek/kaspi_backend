from django.core.validators import MinLengthValidator, MaxLengthValidator
from django.core.exceptions import ValidationError


def length_3(value):
    if len(str(value)) == 3:
        return value
    else:
        raise ValidationError("Length should be 3")


def length_4(value):
    if len(str(value)) == 4:
        return value
    else:
        raise ValidationError("Length should be 4")


def length_16(value):
    if len(str(value)) == 16:
        return value
    else:
        raise ValidationError("Length should be 16")


def validate_credit_card(card_number):
    res = card_number
    card_number = str(card_number)

    # 1. Change datatype to list[int]
    card_number = [int(num) for num in card_number]

    # 2. Remove the last digit:
    checkDigit = card_number.pop(-1)

    # 3. Reverse the remaining digits:
    card_number.reverse()

    # 4. Double digits at even indices
    card_number = [num * 2 if idx % 2 == 0
                   else num for idx, num in enumerate(card_number)]

    # 5. Subtract 9 at even indices if digit is over 9
    # (or you can add the digits)
    card_number = [num - 9 if idx % 2 == 0 and num > 9
                   else num for idx, num in enumerate(card_number)]

    # 6. Add the checkDigit back to the list:
    card_number.append(checkDigit)

    # 7. Sum all digits:
    checkSum = sum(card_number)

    # 8. If checkSum is divisible by 10, it is valid.
    if checkSum % 10 == 0:
        return res
    raise ValidationError("Invalid card number")
